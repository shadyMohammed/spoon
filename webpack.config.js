const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

// /**
//  * Plugins
//  */

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: "style.css"
});

const htmlWebpackPlugin = new HtmlWebpackPlugin({
  filename: "index.html",
  template: "./src/temp/templates/layout.pug"
});

// /**
//  * End of Plugins
//  */

module.exports = {
  entry: "./src/index.js",
  mode: "development",
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.(png|jpg|gif)$/,
        use: "file-loader"
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              plugins: loader => [require("precss"), require("autoprefixer")]
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.pug$/,
        loader: "pug-loader",
        options: {
          pretty: true
        }
      }
    ]
  },
  plugins: [miniCssExtractPlugin, htmlWebpackPlugin]
};
